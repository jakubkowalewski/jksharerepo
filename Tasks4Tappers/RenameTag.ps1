<#
.SYNOPSIS
  Rename tag to specific name
.DESCRIPTION
  Rename tag to specific name
.PARAMETER tagToRename
    Mandatory parameter
    String
    Name of the tag to be renamed
.PARAMETER newTagName
    Mandatory parameter
    String
    Target name of tag to be renamed
.PARAMETER SubID
    Optional parameter
    String
    Subscription ID context
.PARAMETER RemediateOnConflict
    Optional parameter
    Boolean
    for $False show conflict and dont perform any action if conflict
    for $True remediate any conflict with new Tag and New value, only if old tag is empty and new tag exists or old tag has same value as new tag that exists
    Default is $false
.INPUTS
  None
.OUTPUTS
  Hashtable with return values
.NOTES
  Version:        1.2
  Author:         Jakub Kowalewski
  Creation Date:  30.11.2021
  Purpose/Change: Initial script development
  
.EXAMPLE
  powershell.exe -executionpolicy bypass -file .\RenameTag.ps1 -tagToRename <tagToRenameName> -newTagName <targetNameOfTheTag> -subID <subscriptionID> -RemediateOnConflict $false
  powershell.exe -executionpolicy bypass -file .\RenameTag.ps1 -tagToRename <tagToRenameName> -newTagName <targetNameOfTheTag> -RemediateOnConflict $true
  powershell.exe -executionpolicy bypass -file .\RenameTag.ps1 -tagToRename <tagToRenameName> -newTagName <targetNameOfTheTag>
  launch with -Verbose to get Action/Debug info
#>

Param
(
    [Parameter(Mandatory=$true, Position=0)]
    [string] $tagToRename,
    [Parameter(Mandatory=$true, Position=1)]
    [string] $newTagName,
    [Parameter(Mandatory=$false, Position=2)]
    [string[]]$subId,
    [Parameter(Mandatory=$false, Position=3)]
    [Boolean] $RemediateOnConflict = $false
)

Write-Verbose "RemediateState: $RemediateOnConflict"

# Script block that will run async and executes trim action
$fixTagBlock = {
    param($oldTag, $newTag, $resource, $remediate)
    $bckupResource = [Management.Automation.PSSerializer]::DeSerialize([Management.Automation.PSSerializer]::Serialize($($resource | Select-Object *)))
    $tempOldTagVal = $resource.Tags.$oldTag
    #If there are both new and old tag remove old tag if has same value or old tags value is empty
    #$ret = @{}
    if ($resource.Tags.ContainsKey($newTag)) {
        Write-Verbose "NEW KEY PRESENT in addition to old key"
        switch ($remediate) {
            $true {
                Write-Verbose "Remediate!"
                $tempNewTagVal = $resource.Tags.$newTag
                switch ($tempOldTagVal) {
                    {$_ -eq $tempNewTagVal} {
                        $resource.Tags.Remove($oldTag) | Out-Null
                        Write-Verbose "Tag $($newTag) for $($resource.Name) exists and will supersede $($oldTag) because tag key values are the same"
                    }
                    {$_.Length -eq 0} {
                        $resource.Tags.Remove($oldTag) | Out-Null
                        Write-Verbose "Tag $($newTag) for $($resource.Name) exists and will supersede empty $($oldTag) tag"
                    }
                    Default {
                        Write-Verbose "Unspecified situation in switch statement - skipping"
                        $ret = @{
                            Status = 'Skipped'
                            Resource = $bckupResource
                            ResourceModified = ''
                            ErrorMessage = "Skipping $($resource.Name) - old tag $oldTag and new tag $newTag already exist for this resource with different values"
                            ObjType = $resource.GetType().Name
                        }
                        return $ret
                    }
                }
            }
            $false {
                Write-Verbose "DO NOT Remediate!"
                Write-Verbose "Conflict for $($resource.Name) - old tag $oldTag and new tag $newTag already exist for this resource with different values"
                $ret = @{
                    Status = 'Conflict'
                    Resource = $bckupResource
                    ResourceModified = ''
                    ErrorMessage = "Conflict for $($resource.Name) - old tag $oldTag and new tag $newTag already exist for this resource with different values"
                    ObjType = $resource.GetType().Name
                }
                return $ret
            }
        }
    }
    #If there is only old tag, get its value, delete it and assign its value to a new tag
    else {
        $resource.Tags.Remove($oldTag) | Out-Null
        $resource.Tags.$newTag = $tempOldTagVal
    }
    try {
        switch ($resource.GetType().Name) {
            'PSResource' {
                $retVal = $resource | Set-AzResource -Tag $resource.Tags -Force -ErrorAction Stop
                Break
            }
            'PSResourceGroup' {
                $retVal = $resource | Set-AzResourceGroup -Tag $resource.Tags -ErrorAction Stop
                Break
            }
        }
        $ret = @{
            Status = 'Success'
            Resource = $bckupResource
            ResourceModified = $retVal
            ErrorMessage = ''
            ObjType = $resource.GetType().Name
        }
    }
    catch {
        $ret = @{
            Status = 'Fail'
            Resource = $bckupResource
            ResourceModified = ''
            ErrorMessage = $_.Exception.Message
            ObjType = $resource.GetType().Name
        }
    }
    return $ret
}

#Select subs that are provided to perform actions against
function ChooseSubs {
    param (
        [Parameter(Mandatory=$false, Position=0)]
        [string[]]$subId
    )
    $subCount = ($subId | Measure-Object).Count
    Write-Verbose "SubCount: $subCount"
    switch ($subCount) {
        0 {
            $subsObjs = Get-AzSubscription
            Write-Verbose 'SubId is 0'
            Break
        }
        Default {
            $subsObjs = $subId | ForEach-Object { Get-AzSubscription -SubscriptionId $_ }
            Write-Verbose 'SubId is default (0 < x)'
            Break
        }
    }
    return $subsObjs
}

# This function gets all resources and resource groups for given tag name and given subscriptions
function Get-AllPresentTags {
    param (
        [Parameter(Mandatory=$true, Position=0)]
        [string]$tagToRename,
        [Parameter(Mandatory=$true, Position=1)]
        [Object[]]$subsList
    )

    Begin{
        $allRes = @()
    }

    Process{
        $subsList | ForEach-Object {
            Set-AzContext -SubscriptionObject $_ | Out-Null
            $allRes += Get-AzResource -TagName $tagToRename
            $allRes += Get-AzResourceGroup -Tag @{"$($tagToRename)"=""}
        }
        $resCount = ($allRes | Measure-Object).Count
        if($resCount -eq 0){
            Write-Error "No such Tag Name found."
            exit 1
        }
        else{
            Write-Verbose "Performing action for $resCount elements"
        }
    }

    End{
        return $allRes
    }
}

# Script Entry Point
# Initialize runspace for parellar execution
$RunspacePool = [runspacefactory]::CreateRunspacePool(
    [System.Management.Automation.Runspaces.InitialSessionState]::CreateDefault()
)
$RunspacePool.SetMinRunspaces(1) | Out-Null
$RunspacePool.SetMaxRunspaces([int]$env:NUMBER_OF_PROCESSORS + 1) | Out-Null
$RunspacePool.ApartmentState = "MTA"
$RunspacePool.ThreadOptions = 2
<#
Default	0	
Use the default options: UseNewThread for local Runspace, ReuseThread for local RunspacePool, server settings for remote Runspace and RunspacePool
ReuseThread	2	
Creates a new thread for the first invocation and then re-uses that thread in subsequent invocations.
UseCurrentThread	3	
Doesn't create a new thread; the execution occurs on the thread that calls Invoke.
UseNewThread	1	
Creates a new thread for each invocation
#>
$RunspacePool.Open()
$Jobs = New-Object System.Collections.ArrayList

# Initialize stuff for script logic
$resArr = @() # Init Array for storing found resources

if($null -eq $subId){
    $fetchSubs = ChooseSubs
    Write-Verbose 'SubId is null'
}
else{
    $fetchSubs = ChooseSubs -subId $subId
    Write-Verbose 'SubId IS NOT null'
}
Write-Verbose "fetchsubs has $(($fetchSubs | Measure-Object).Count)"
$resByIds = [System.Collections.ArrayList]@() # Init array for storing arrays - without fixed size in memory
$resArr = Get-AllPresentTags -tagToRename $tagToRename -subsList $fetchSubs # Get resources into array
$resCount = ($resArr | Measure-Object).Count
Write-Verbose "Tags present in $resCount resources"

#Select only subs that resources were found for
$avalSubsForExisting = $resArr | ForEach-Object { $_.ResourceId.Split('/')[2] } | Get-Unique
$outRet = [System.Collections.ArrayList]@() # Init array for storing arrays - without fixed size in memory

foreach($subIdEl in $avalSubsForExisting){
    $tempArr = $resArr | Where-Object { $_.ResourceId -like "*$subIdEl*" }
    $resByIds.Add($tempArr) | Out-Null
}

[int]$counter = 0
foreach($resSubGroup in $resByIds){
    Write-Verbose "ResByIds $(($resByIds | Measure-Object).Count)"
    Write-Verbose "resSubGroup $(($resSubGroup | Measure-Object).Count)"
    $resSubId = $($resSubGroup.ResourceId.Split('/')[2])
    Set-AzContext -SubscriptionId $resSubId | Out-Null
    foreach($resEl in $resSubGroup){
        switch ($resEl.GetType().Name) {
            'PSResource' {
                Write-Verbose "Creating runspace for $($resEl.Name)"
            }
            'PSResourceGroup' {
                Write-Verbose "Creating runspace for $($resEl.ResourceGroupName)"
            }
        }
        $paramsToAdd = @{
            oldTag = $tagToRename
            newTag = $newTagName
            resource = $resEl
            remediate = $RemediateOnConflict
        }
        $PowerShell = [powershell]::Create()
        $PowerShell.RunspacePool = $RunspacePool
        $PowerShell.AddScript($fixTagBlock, $true).AddParameters($paramsToAdd) | Out-Null
        $JobObj = New-Object -TypeName PSObject -Property @{
            Runspace = $PowerShell.BeginInvoke()
            PowerShell = $PowerShell
        }
        $Jobs.Add($JobObj) | Out-Null
    }
    while ($Jobs.Runspace.IsCompleted -contains $false) {
        $percent = $($counter / $resCount)*100
        Write-Verbose "Percent: $percent"
        Write-Progress -Activity "Renaming tag $tagToRename to $newTagName" -Status "$counter out of $resCount completed" -PercentComplete $percent
        if($Jobs.Runspace.IsCompleted -contains $true){
            $tempJobCollector = @()
            $tempJobCollector = $Jobs | Where-Object { $_.Runspace.IsCompleted -eq $true }
            #Gather all output from finished runspace instances
            foreach($el in $tempJobCollector){
                $outRet.Add($($el.Powershell.EndInvoke($el.Runspace))) | Out-Null
                $el.PowerShell.Dispose() | Out-Null
                $Jobs.Remove($($Jobs | Where-Object { $_.Runspace.AsyncWaitHandle.Handle -eq $el.Runspace.AsyncWaitHandle.Handle }))
                $counter++
            }
        }
    }
}

$RunspacePool.Close() | Out-Null
$RunspacePool.Dispose() | Out-Null

#Create nice output
$outPut = [System.Collections.ArrayList]@() # Init array for storing arrays - without fixed size in memory
foreach($outJobEl in $outRet){
    switch ($outJobEl.Status) {
        'Success' {
            $outPut += [PSCustomObject] @{
                SubID = $outJobEl.Resource.ResourceId.Split('/')[2]
                ResourceGroupName = $outJobEl.Resource.ResourceGroupName
                Resource = $outJobEl.Resource.ResourceName
                ResourceType = $outJobEl.ObjType
                OldTagName = $tagToRename
                NewTagName = $newTagName
                OldTagValue = $outJobEl.Resource.Tags.$tagToRename
                NewTagValue = $outJobEl.ResourceModified.Tags.$newTagName
                Status = $outJobEl.Status
                ErrorMessage = ''
            }
            Break
        }
        'Fail' {
            $outPut += [PSCustomObject] @{
                SubID = $outJobEl.Resource.ResourceId.Split('/')[2]
                ResourceGroupName = $outJobEl.Resource.ResourceGroupName
                Resource = $outJobEl.Resource.ResourceName
                ResourceType = $outJobEl.ObjType
                OldTagName = $tagToRename
                NewTagName = ''
                OldTagValue = $outJobEl.Resource.Tags.$newTagName
                NewTagValue = ''
                Status = $outJobEl.Status
                ErrorMessage = $outJobEl.ErrorMessage
            }
            Break
        }
        'Conflict' {
            $outPut += [PSCustomObject] @{
                SubID = $outJobEl.Resource.ResourceId.Split('/')[2]
                ResourceGroupName = $outJobEl.Resource.ResourceGroupName
                Resource = $outJobEl.Resource.ResourceName
                ResourceType = $outJobEl.ObjType
                OldTagName = $tagToRename
                NewTagName = $newTagName
                OldTagValue = $outJobEl.Resource.Tags.$tagToRename
                NewTagValue = $outJobEl.Resource.Tags.$newTagName
                Status = $outJobEl.Status
                ErrorMessage = $outJobEl.ErrorMessage
            }
            Break
        }
        'Skipped' {
            $outPut += [PSCustomObject] @{
                SubID = $outJobEl.Resource.ResourceId.Split('/')[2]
                ResourceGroupName = $outJobEl.Resource.ResourceGroupName
                Resource = $outJobEl.Resource.ResourceName
                ResourceType = $outJobEl.ObjType
                OldTagName = $tagToRename
                NewTagName = $newTagName
                OldTagValue = $outJobEl.Resource.Tags.$tagToRename
                NewTagValue = $outJobEl.Resource.Tags.$newTagName
                Status = $outJobEl.Status
                ErrorMessage = $outJobEl.ErrorMessage
            }
            Break
        }
    }
}

#Comment this line to get rid of result popup
$outPut | Out-GridView

return $outPut