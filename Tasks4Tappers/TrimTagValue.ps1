<#
.SYNOPSIS
  Trim given tag value
.DESCRIPTION
  Trim given tag value
.PARAMETER tagToTrim
    Mandatory parameter
    Name of the tag to be renamed
.PARAMETER SubID
    Optional parameter
    Subscription ID context
.INPUTS
  None
.OUTPUTS
  Hashtable with return values
.NOTES
  Version:        1.3
  Author:         Jakub Kowalewski
  Creation Date:  30.11.2021
  Purpose/Change: Initial script development
  
.EXAMPLE
  powershell.exe -executionpolicy bypass -file .\TrimTagValue.ps1 -tagToTrim <tagToTrimName> -subID <subscriptionID>
  powershell.exe -executionpolicy bypass -file .\TrimTagValue.ps1 -tagToTrim <tagToTrimName>
  launch with -Verbose to get Action/Debug info
#>

Param
(
    [Parameter(Mandatory=$true, Position=0)]
    [string]$tagToTrim,
    [Parameter(Mandatory=$false, Position=1)]
    [string[]]$subId
)

#$tagToTrim = 'Kupa58'

# Script block that will run async and executes trim action
$trimTagBlock = {
    param($tag, $resource)
    $bckupResource = [Management.Automation.PSSerializer]::DeSerialize([Management.Automation.PSSerializer]::Serialize($resource))
    $tempTagVal = $resource.Tags.$tag
    Write-Verbose "TagValB4Trim: `"$tempTagVal`""
    $resource.Tags.Remove($tag) | Out-Null
    $resource.Tags.$tag = $tempTagVal.Trim()
    try {
        switch ($resource.GetType().Name) {
            'PSResource' {
                $retVal = $resource | Set-AzResource -Tag $resource.Tags -Force -ErrorAction Stop
            }
            'PSResourceGroup' {
                $retVal = $resource | Set-AzResourceGroup -Tag $resource.Tags -ErrorAction Stop
            }
        }
        $ret = @{
            Status = 'Success'
            Resource = $bckupResource
            ResourceModified = $retVal
            ErrorMessage = ''
            ObjType = $resource.GetType().Name
        }
    }
    catch {
        $ret = @{
            Status = 'Fail'
            Resource = $bckupResource
            ResourceModified = ''
            ErrorMessage = $_.Exception.Message
            ObjType = $resource.GetType().Name
        }
    }
    return $ret
}

#Select subs that are provided to perform actions against
function ChooseSubs {
    param (
        [Parameter(Mandatory=$false, Position=0)]
        [string[]]$subId
    )
    $subCount = ($subId | Measure-Object).Count
    Write-Verbose "SubCount: $subCount"
    switch ($subCount) {
        0 {
            $subsObjs = Get-AzSubscription
            Write-Verbose 'SubId is 0'
        }
        Default {
            $subsObjs = $subId | ForEach-Object { Get-AzSubscription -SubscriptionId $_ }
            Write-Verbose 'SubId is default (0 < x)'
        }
    }
    return $subsObjs
}

# This function gets all resources and resource groups for given tag name and given subscriptions
function Get-AllPresentTags {
    param (
        [Parameter(Mandatory=$true, Position=0)]
        [string]$tagToTrim,
        [Parameter(Mandatory=$true, Position=1)]
        [Object[]]$subsList
    )

    Begin{
        $allRes = @()
        $regexPattern = '^(\S+)(\s*\S+)*?$'
    }

    Process{
        $subsList | ForEach-Object {
            Set-AzContext -SubscriptionObject $_ | Out-Null
            $allRes += Get-AzResource -TagName $tagToTrim | Where-Object { $_.Tags.$tagToTrim -notmatch $regexPattern }
            $allRes += Get-AzResourceGroup -Tag @{"$($tagToTrim)"=""} | Where-Object { $_.Tags.$tagToTrim -notmatch $regexPattern }
        }
        $resCount = ($allRes | Measure-Object).Count
        if($resCount -eq 0){
            Write-Error "No such Tag Name found that needs to have value trimmed."
            exit 1
        }
        else{
            Write-Verbose "Performing action for $resCount elements"
        }
    }

    End{
        return $allRes
    }
}

# Script Entry Point
# Initialize runspace for parellar execution
$RunspacePool = [runspacefactory]::CreateRunspacePool(
    [System.Management.Automation.Runspaces.InitialSessionState]::CreateDefault()
)
$RunspacePool.SetMinRunspaces(1) | Out-Null
$RunspacePool.SetMaxRunspaces([int]$env:NUMBER_OF_PROCESSORS + 1) | Out-Null
$RunspacePool.ApartmentState = "MTA"
$RunspacePool.ThreadOptions = 2
<#
Default	0	
Use the default options: UseNewThread for local Runspace, ReuseThread for local RunspacePool, server settings for remote Runspace and RunspacePool
ReuseThread	2	
Creates a new thread for the first invocation and then re-uses that thread in subsequent invocations.
UseCurrentThread	3	
Doesn't create a new thread; the execution occurs on the thread that calls Invoke.
UseNewThread	1	
Creates a new thread for each invocation
#>
$RunspacePool.Open()
$Jobs = New-Object System.Collections.ArrayList

# Initialize stuff for script logic
$resArr = @() # Init Array for storing found resources

if($null -eq $subId){
    $fetchSubs = ChooseSubs
    Write-Verbose 'SubId is null'
}
else{
    $fetchSubs = ChooseSubs -subId $subId
    Write-Verbose 'SubId IS NOT null'
}
Write-Verbose "fetchsubs has $(($fetchSubs | Measure-Object).Count)"
$resByIds = [System.Collections.ArrayList]@() # Init array for storing arrays - without fixed size in memory
$resArr = Get-AllPresentTags -tagToTrim $tagToTrim -subsList $fetchSubs # Get resources into array
$resCount = ($resArr | Measure-Object).Count
Write-Verbose "Tags present in $resCount resources"

#Select only subs that resources were found for
$avalSubsForExisting = $resArr | ForEach-Object { $_.ResourceId.Split('/')[2] } | Get-Unique
$outRet = [System.Collections.ArrayList]@() # Init array for storing arrays - without fixed size in memory

foreach($subIdEl in $avalSubsForExisting){
    $tempArr = $resArr | Where-Object { $_.ResourceId -like "*$subIdEl*" }
    $resByIds.Add($tempArr) | Out-Null
}

[int]$counter = 0
foreach($resSubGroup in $resByIds){
    Write-Verbose "ResByIds $(($resByIds | Measure-Object).Count)"
    Write-Verbose "resSubGroup $(($resSubGroup | Measure-Object).Count)"
    $resSubId = $($resSubGroup.ResourceId.Split('/')[2])
    Set-AzContext -SubscriptionId $resSubId | Out-Null
    foreach($resEl in $resSubGroup){
        switch ($resEl.GetType().Name) {
            'PSResource' {
                Write-Verbose "Creating runspace for $($resEl.Name)"
            }
            'PSResourceGroup' {
                Write-Verbose "Creating runspace for $($resEl.ResourceGroupName)"
            }
        }
        $paramsToAdd = @{
            tag = $tagToTrim
            resource = $resEl
        }
        $PowerShell = [powershell]::Create()
        $PowerShell.RunspacePool = $RunspacePool
        $PowerShell.AddScript($trimTagBlock, $true).AddParameters($paramsToAdd) | Out-Null
        $JobObj = New-Object -TypeName PSObject -Property @{
            PowerShell = $PowerShell
            Runspace = $PowerShell.BeginInvoke()
        }
        $Jobs.Add($JobObj) | Out-Null
    }
    while ($Jobs.Runspace.IsCompleted -contains $false) {
        $percent = $($counter / $resCount)*100
        Write-Verbose "Percent: $percent"
        Write-Progress -Activity "Trimming tag $tagToTrim" -Status "$counter out of $resCount completed" -PercentComplete $percent
        if($Jobs.Runspace.IsCompleted -contains $true){
            $tempJobCollector = @()
            $tempJobCollector = $Jobs | Where-Object { $_.Runspace.IsCompleted -eq $true }
            #Gather all output from finished runspace instances
            foreach($el in $tempJobCollector){
                $outRet.Add($($el.Powershell.EndInvoke($el.Runspace))) | Out-Null
                $el.PowerShell.Dispose() | Out-Null
                $Jobs.Remove($($Jobs | Where-Object { $_.Runspace.AsyncWaitHandle.Handle -eq $el.Runspace.AsyncWaitHandle.Handle }))
                $counter++
            }
        }
    }
}

$RunspacePool.Close() | Out-Null
$RunspacePool.Dispose() | Out-Null

#Create nice output
$outPut = [System.Collections.ArrayList]@() # Init array for storing arrays - without fixed size in memory
foreach($outJobEl in $outRet){
    switch ($outJobEl.Status) {
        'Success' {
            $outPut += [PSCustomObject] @{
                SubID = $outJobEl.Resource.ResourceId.Split('/')[2]
                ResourceGroupName = $outJobEl.Resource.ResourceGroupName
                Resource = $outJobEl.Resource.ResourceName
                ResourceType = $outJobEl.ObjType
                OldTagName = $tagToTrim
                NewTagName = ''
                OldTagValue = $outJobEl.Resource.Tags.$tagToTrim
                NewTagValue = $outJobEl.ResourceModified.Tags.$tagToTrim
                Status = $outJobEl.Status
                ErrorMessage = ''
            }
        }
        'Fail' {
            $outPut += [PSCustomObject] @{
                SubID = $outJobEl.Resource.ResourceId.Split('/')[2]
                ResourceGroupName = $outJobEl.Resource.ResourceGroupName
                Resource = $outJobEl.Resource.ResourceName
                ResourceType = $outJobEl.ObjType
                OldTagName = $tagToTrim
                NewTagName = ''
                OldTagValue = $outJobEl.Resource.Tags.$tagToTrim
                NewTagValue = ''
                Status = $outJobEl.Status
                ErrorMessage = $outJobEl.ErrorMessage
            }
        }
    }
}

#Comment this line to get rid of result popup
$outPut | Out-GridView

return $outPut